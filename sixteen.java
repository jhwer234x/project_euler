import java.math.BigInteger;

public class sixteen {
    public static void main(String[] args) {

        BigInteger multiplier = BigInteger.valueOf(1);
        for (int i = 0; i < 1000; i++) {
            multiplier = multiplier.multiply(BigInteger.valueOf(2));
        }
        String digit = multiplier.toString();

        int answer = 0;
        for (int i = 0; i < digit.length(); i++) {
            answer += Integer.parseInt(String.valueOf(digit.charAt(i)));
        }
        System.out.println("answer = " + answer);
    }
}
