import java.math.BigInteger;

public class fifteen {

    public static BigInteger factorial(BigInteger b) {
        if (b.compareTo(BigInteger.valueOf(0)) == 0) return BigInteger.valueOf(1);
        else {
            BigInteger one = BigInteger.valueOf(1);
            BigInteger subtracted = b.subtract(one);
            return b.multiply(factorial(subtracted));
        }
    }

    public static void main(String args[]) {
        //N choose n
        int n = 20;
        BigInteger topFraction = factorial(BigInteger.valueOf(2 * 20));
        BigInteger bottomFraction = factorial(BigInteger.valueOf(20));
        BigInteger answer = topFraction.divide(bottomFraction.multiply(bottomFraction));

        System.out.println("answer = " + answer);
    }
}
